# chrisandrikanich.com
## Project Repo
Code is hosted at [gitlab.com](https://gitlab.com/candrikanich).
At this time, all pushes to `master` will deploy a new site.
## Project Server
The site is deployed and served by [netlify.com](https://app.netlify.com/).
 ## Project Domain
This site URL is [chrisandrikanich.com](https://www.chrisandrikanich.com) and the domain is hosted with [Google Domains](https://domains.google.com).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run create
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

[![Netlify Status](https://api.netlify.com/api/v1/badges/6cda987b-c87a-428a-b79d-8f84193f14fa/deploy-status)](https://app.netlify.com/sites/chris-andrikanich/deploys)

