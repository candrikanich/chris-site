import Vue from 'vue'
import App from './App.vue'
import router from './router'
import "tailwindcss/tailwind.css"
import VueI18n from 'vue-i18n'
import locales from './locales.json'

Vue.use(VueI18n)

// replace with json file
// const messages = {
//   en: {
//     message: {
//       value: 'This is an example of content translation.'
//     }
//   },
//   be: {
//     message: {
//       value: 'Гэта прыклад перакладу змесціва.'
//     }
//   },
//   da: {
//     message: {
//       value: 'Dette er et eksempel på oversættelse af indhold.'
//     }
//   },
//   hr: {
//     message: {
//       value: 'Ovo je primjer prevođenja sadržaja.'
//     }
//   }
// };

const i18n = new VueI18n({
  // locale: 'en',
  locale: navigator.language,
  messages: locales
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
  i18n
}).$mount('#app')
